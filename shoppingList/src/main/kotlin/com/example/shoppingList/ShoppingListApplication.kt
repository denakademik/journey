package com.example.shoppingList

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication


@SpringBootApplication
class ShoppingListApplication

fun main(args: Array<String>) {
	SpringApplication.run(ShoppingListApplication::class.java, *args)
}
