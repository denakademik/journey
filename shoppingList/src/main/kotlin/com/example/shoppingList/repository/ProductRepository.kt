package com.example.shoppingList.repository

import com.example.shoppingList.Models.Product
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository


  interface ProductRepository : CrudRepository<Product, Long>
