package com.example.shoppingList.Models

import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.persistence.*

@Entity
@Table(name = "categories")
class Category (

    @Id
    @JsonProperty("id")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long = 0L,

    @JsonProperty("name")
    @Column(name = "name")
    val name: String = "",

    @JsonProperty("description")
    @Column(name = "description", length = 1000)
    val description: String = ""
)