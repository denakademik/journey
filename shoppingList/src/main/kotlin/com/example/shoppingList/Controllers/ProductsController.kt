package com.example.shoppingList.Controllers

import com.example.shoppingList.Models.Product
import com.example.shoppingList.service.ProductService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("products")
class ProductsController (private val productService: ProductService ){

    @GetMapping
    fun index() =productService.all();

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody product: Product) = productService.add(product)

}